<?php
/*
 * YARPP's built-in thumbnails template
 * @since 4
 *
 * This template is used when you choose the built-in thumbnails option.
 * If you want to create a new template, look at yarpp-templates/yarpp-template-example.php as an example.
 * More information on the custom templates is available at http://mitcho.com/blog/projects/yarpp-3-templates/
 */

if ( !$this->diagnostic_using_thumbnails() )
	$this->set_option( 'manually_using_thumbnails', true );

$options = array( 'thumbnails_heading', 'thumbnails_default', 'no_results' );
extract( $this->parse_args( $args, $options ) );

// a little easter egg: if the default image URL is left blank,
// default to the theme's header image. (hopefully it has one)
if ( empty($thumbnails_default) )
	$thumbnails_default = get_header_image();

$dimensions = $this->thumbnail_dimensions();

$output .= '<h4>其他有趣的文章</h4>' . "\n";

if (have_posts()) {
	
	while (have_posts()) {
		the_post();

		$output .= "<a class='yarpp-thumbnail-sidebar' href='" . get_permalink() . "' title='" . the_title_attribute('echo=0') . "'>" . "\n";

		$post_thumbnail_html = '';
		if ( has_post_thumbnail() ) {
			if ( $this->diagnostic_generate_thumbnails() )
				$this->ensure_resized_post_thumbnail( get_the_ID(), $dimensions );
			$post_thumbnail_html = get_the_post_thumbnail( null, $dimensions['size'] );
		}
		
		if ( trim($post_thumbnail_html) != '' )
			$output .= $post_thumbnail_html;


		else
		$output .= '<span class="yarpp-thumbnail-default"><img src="' . esc_url($thumbnails_default) . '"/></span>';

		$output .= '<span class="yarpp-thumbnail-title">' . get_the_title() . '</span>';

		$output .= '</a>' . "\n";

	}
	
} else { ?>

<a class="yarpp-thumbnail-sidebar" href="http://localhost/wanka365.com/world/asia/taiwan-hakka-village/" title="到客家小镇作客">
<img width="120" height="120" src="http://localhost/wanka365.com/wp-content/uploads/2014/01/9-120x120.jpg" class="attachment-yarpp-thumbnail wp-post-image" alt="9"><span class="yarpp-thumbnail-title">到客家小镇作客</span></a>
<a class="yarpp-thumbnail-sidebar" href="http://localhost/wanka365.com/world/asia/in-love-with-bali/" title="恋上5米3分钟巴厘岛">
<img width="120" height="120" src="http://localhost/wanka365.com/wp-content/uploads/2014/03/BIGP1013410-120x120.jpg" class="attachment-yarpp-thumbnail wp-post-image" alt="OLYMPUS DIGITAL CAMERA"><span class="yarpp-thumbnail-title">恋上5米3分钟巴厘岛</span></a>
<a class="yarpp-thumbnail-sidebar" href="http://localhost/wanka365.com/world/asia/harran-beehive-house/" title="入住哈兰蜂巢泥屋村落">
<img width="120" height="120" src="http://localhost/wanka365.com/wp-content/uploads/2014/03/PA316538-120x120.jpg" class="attachment-yarpp-thumbnail wp-post-image" alt="OLYMPUS DIGITAL CAMERA"><span class="yarpp-thumbnail-title">入住哈兰蜂巢泥屋村落</span></a>
<a class="yarpp-thumbnail-sidebar" href="http://localhost/wanka365.com/world/asia/%e7%a2%a7%e6%b5%b7%e8%93%9d%e5%a4%a9%e9%be%9f%e5%b2%9b/" title="碧海蓝天龟岛">
<img width="120" height="120" src="http://localhost/wanka365.com/wp-content/uploads/2014/03/BIGDSC_3467-120x120.jpg" class="attachment-yarpp-thumbnail wp-post-image" alt="龟岛地标，背景为Ko Naang Yuan"><span class="yarpp-thumbnail-title">碧海蓝天龟岛</span></a>

<?php }

$this->enqueue_thumbnails( $dimensions );


