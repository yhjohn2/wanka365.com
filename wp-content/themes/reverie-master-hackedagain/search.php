<?php get_header(); ?>

<!-- Row for main content area -->
	<div class="small-12 medium-push-2 medium-10 large-7 large-push-2 columns" role="main">
	<div class="cat-title">
		<h2><?php _e('搜索:', 'reverie'); ?> "<?php echo get_search_query(); ?>"</h2>

	</div>
	
	<div id="content">
	
	<?php if ( have_posts() ) : ?>
	
		<?php /* Start the Loop */ ?>
		<?php while ( have_posts() ) : the_post(); ?>
			<?php get_template_part( 'content', get_post_format() ); ?>
		<?php endwhile; ?>
		
		<?php else : ?>
			
			<h3>对不起，您的搜索“<?php echo get_search_query(); ?>”没有结果。或许你对以下的文章有兴趣：</h3>
			<p />
<hr />


<?php $posts = get_posts('orderby=rand&numberposts=5'); foreach($posts as $post) { ?>

<article id="post-<?php the_ID(); ?>" <?php post_class('index-card'); ?>>
	<header>
		<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
		<?php reverie_entry_meta(); ?>
	</header>
	<div class="entry-content">
		<figure><a href="<?php the_permalink(); ?>"><?php if ( has_post_thumbnail() ) {the_post_thumbnail('featured'); } ?></a></figure> 
		<?php the_excerpt(); ?>
		<div class="readmore"><a href="<?php the_permalink(); ?>">继续阅读</a></div>
	</div>
</article>


<?php } ?>



			
		
	<?php endif; // end have_posts() check ?>
	
	<?php /* Display navigation to next/previous pages when applicable */ ?>
	<?php if ( function_exists('reverie_pagination') ) { reverie_pagination(); } else if ( is_paged() ) { ?>
		<nav id="post-nav">
			<div class="post-previous"><?php next_posts_link( __( '&larr; Older posts', 'reverie' ) ); ?></div>
			<div class="post-next"><?php previous_posts_link( __( 'Newer posts &rarr;', 'reverie' ) ); ?></div>
		</nav>
	<?php } ?>

	</div>
	</div>
	<?php get_sidebar('left'); ?>
	<?php get_sidebar('right'); ?>
	<?php get_sidebar('right-instagram'); ?>
		
<?php get_footer(); ?>