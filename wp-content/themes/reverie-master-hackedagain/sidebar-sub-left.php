<aside id="sidebar-left" class="small-12 medium-2 medium-pull-10 large-2 large-pull-7 columns">
	
 	<div class="panel">
		<h4>作者介绍</h4>
		
			<div class="row">
				<div class="small-12 columns">
					<?php echo get_avatar( get_the_author_meta('user_email'), 95 ); ?>
				</div>
				<div class="author_name">
				<div class="small-12 columns">
					
				<?php the_author_posts_link(); ?>
				</div>
					
				</div>
			</div>

			<div class="row">
				<div class="small-12 columns">

					<p class="cover-description"><?php the_author_meta('description'); ?></p>

					
				</div>
			</div>


	</div>
	<div class="panel">
			<h4>文章标签</h4>
	<div class="row">
				<div class="small-12 tags">
			<?php the_tags('<ul><li>','</li><li>','</li></ul>','</li></ul>','</li></ul>'); ?>
			</div>

	</div>
	</div>
	

	<?php dynamic_sidebar("Sidebar-sub-left"); ?>
</aside><!-- /#sidebar -->