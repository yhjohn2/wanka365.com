<?php
/**
 * The template for displaying a "No posts found" message.
 *
 * @subpackage Reverie
 * @since Reverie 4.0
 */
?>

<article id="post-0" class="post no-results not-found">
	<header>
		<h2><?php _e( 'Nothing Found', 'reverie' ); ?></h2>
	</header>
	<div class="entry-content">
		<p><?php _e( 'Apologies, but no results were found. Perhaps searching will help find a related post.', 'reverie' ); ?></p>

	</div>
	<hr />
	<article id="post-<?php the_ID(); ?>" <?php post_class('index-card'); ?>>
	<header>
		<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
		<?php reverie_entry_meta(); ?>
	</header>
	<div class="entry-content">
		<figure><a href="<?php the_permalink(); ?>"><?php if ( has_post_thumbnail() ) {the_post_thumbnail('featured'); } ?></a></figure> 
		<?php the_excerpt(); ?>
		<div class="readmore"><a href="<?php the_permalink(); ?>">继续阅读</a></div>
	</div>
</article>

</article>