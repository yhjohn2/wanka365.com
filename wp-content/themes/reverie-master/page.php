<?php get_header(); ?>

<!-- Row for main content area -->
	<div class="small-12 medium-push-2 medium-10 large-7 large-push-2 columns" id="content" role="main">
	
	
	<?php /* Start loop */ ?>
	<?php while (have_posts()) : the_post(); ?>
		<article <?php post_class() ?> id="post-<?php the_ID(); ?>">
			<header>
				<h1 class="entry-title"><?php the_title(); ?></h1>
				<?php reverie_entry_meta(); ?>
			</header>
			<div class="entry-content">
				<?php the_content(); ?>
			</div>
			<footer>
				<?php wp_link_pages(array('before' => '<nav id="page-nav"><p>' . __('Pages:', 'reverie'), 'after' => '</p></nav>' )); ?>
			</footer>
		</article>
	<?php endwhile; // End the loop ?>

	</div>
	
	<?php get_sidebar('left'); ?>
	<?php get_sidebar('right'); ?>
	<?php get_sidebar('right-instagram'); ?>
		
<?php get_footer(); ?>