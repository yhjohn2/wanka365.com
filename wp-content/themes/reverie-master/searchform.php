<div class="search">
<div id="sb-search" class="sb-search">
	<form role="search" method="get" id="searchform" action="<?php echo home_url('/'); ?>">
		<input class="sb-search-input" placeholder="搜索" type="text" value="" name="s" id="s">
		<input class="sb-search-submit" type="submit" value="<?php esc_attr_e('Search', 'reverie'); ?>" id="searchsubmit" >
		<span class="sb-icon-search"></span>
	</form>
</div>
</div>