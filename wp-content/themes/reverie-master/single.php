<?php get_header(); ?>

<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.min.2.0.3.js"></script>
	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.jscrollpane.min.js"></script>


<!-- Row for main content area -->
	<div class="small-12 medium-push-2 medium-10 large-7 large-push-2 columns" id="content" role="main">
	
	<?php /* Start loop */ ?>
	<?php while (have_posts()) : the_post(); ?>



		<article <?php post_class() ?> id="post-<?php the_ID(); ?>">
			<header>
				<h1 class="entry-title"><?php the_title(); ?></h1>
				<?php reverie_entry_meta(); ?>
			</header>
			<div class="entry-content">

				<?php 
				//check if is it is a video, if yes, add additional div for responsiveness
				if ( has_post_format( 'video' )) {
					echo '<div class="video-container">';
					the_content();
					echo '</div>';

				} else { 
					the_content();
				}?>


				
				<?php custom_wp_link_pages(array('before' => '<nav id="page-nav"><p>' . __('<b>页面: </b>', 'reverie'), 'after' => '</p></nav>' )); ?>
				
			</div>
			<div class="addthis">
			<!-- AddThis Button BEGIN -->
<div class="addthis_toolbox addthis_default_style addthis_32x32_style">
<a class="addthis_button_preferred_1"></a>
<a class="addthis_button_preferred_2"></a>
<a class="addthis_button_preferred_3"></a>
<a class="addthis_button_preferred_4"></a>
<a class="addthis_button_compact"></a>
<a class="addthis_counter addthis_bubble_style"></a>
</div>
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-535f62d46200c12c"></script>
<!-- AddThis Button END -->
</div>
			
			<?php related_posts() ?>

			<div class="fbcomments">
			<?php  echo do_shortcode('[fbcomments width="500"]');  ?>
		</div>

			<footer>
				
				<?php edit_post_link('Edit this Post'); ?>
			</footer>
		</article>
		
		<?php//comments_template(); ?>
	<?php endwhile; // End the loop ?>

	</div>
	<?php get_sidebar('sub-left'); ?>
	<?php get_sidebar('right'); ?>
	<?php get_sidebar('right-instagram'); ?>
	



		
<?php get_footer(); ?>