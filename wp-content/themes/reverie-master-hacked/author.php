<?php get_header(); ?>

<!-- Row for main content area -->


	<div class="small-12 medium-push-2 medium-10 large-7 large-push-2 columns article-categories" role="main">
		<div class="cat-title">
		<h2>作者<?php the_author(); //single_cat_title(); ?>的文章</h2>
		</div>

	<div id="content">
	<?php if ( have_posts() ) : ?>
		<?php /* Start the Loop */ ?>
		<?php while ( have_posts() ) : the_post(); ?>
			<?php get_template_part( 'content', get_post_format() ); ?>
		<?php endwhile; ?>
		
		<?php else : ?>
			<?php get_template_part( 'content', 'none' ); ?>
		
	<?php endif; // end have_posts() check ?>
	
	<?php /* Display navigation to next/previous pages when applicable */ ?>
	<?php if ( function_exists('reverie_pagination') ) { reverie_pagination(); } else if ( is_paged() ) { ?>
		<nav id="post-nav">
			<div class="post-previous"><?php next_posts_link( __( '&larr; Older posts', 'reverie' ) ); ?></div>
			<div class="post-next"><?php previous_posts_link( __( 'Newer posts &rarr;', 'reverie' ) ); ?></div>
		</nav>
	<?php } ?>

	</div>
	</div>
	<?php get_sidebar('left'); ?>
	<?php get_sidebar('right'); ?>
	<?php get_sidebar('right-instagram'); ?>		
<?php get_footer(); ?>