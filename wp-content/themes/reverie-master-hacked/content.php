<?php
/**
 * The default template for displaying content. Used for both single and index/archive/search.
 *
 * @subpackage Reverie
 * @since Reverie 4.0
 */
?>

<div class="post-cat-label">
<?php 
$category = get_the_category(); 
if($category[0]){
//echo '<a href="'.get_category_link($category[0]->term_id ).'"><i class="fa fa-compass"></i> '.$category[0]->cat_name.'</a>';
}
?>
</div>

<article id="post-<?php the_ID(); ?>" <?php post_class('index-card'); ?>>
	<header>
		<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
		<?php reverie_entry_meta(); ?>
	</header>
	<div class="entry-content">
		<figure><a href="<?php the_permalink(); ?>"><?php if ( has_post_thumbnail() ) {the_post_thumbnail('featured'); } ?></a></figure> 
		<?php the_excerpt(); ?>
		<div class="readmore"><a href="<?php the_permalink(); ?>">继续阅读</a></div>
	</div>
</article>

                   
